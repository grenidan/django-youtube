from django.shortcuts import render
from .models import Video
from django.views.decorators.csrf import csrf_exempt
#from .ytchannel import Video

def index(request):

    lista_videos = Video.objects.all()
    context = {'lista':lista_videos}
    if request.method == "POST":
        id = request.POST['action']
        video = Video.objects.get(id=id)
        seleccionado = video.seleccionado
        video.seleccionado = not seleccionado
        video.save()

    return render(request, "parserapp/index.html", context)