from django.db import models

class Video(models.Model):
    nombre = models.CharField(max_length=64)
    link = models.CharField(max_length=64)
    seleccionado = models.BooleanField(default=False)
    id = models.CharField(max_length=12, primary_key=True)

    def __str__(self):
        return self.nombre